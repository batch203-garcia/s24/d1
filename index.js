//ES26 -> ECMAScript 2015 -> standard that is used to create implementation of the language -> one which is JS -> all browser vendor can implement.

const firstNum = Math.pow(8, 3);
console.log(firstNum);

const secondNum = 8 ** 3;
console.log(secondNum);

//Template Literals
//${} -> placeholder for template literals

let name = "John";

//Pre-template literals

let message = "Hello " + name + "! Welcome to programming!";

console.log("Message without template literals: ");
console.log(message);

//Strings using template literals -> backticks (``)

message = `Hello ${name}! Welcome to programming!`

//template literals
console.log(message);

const anotherMessage = `${name} attended a math competetion.
He won it by solving the problem "8 ** 2" with a solution of ${8**2};`
console.log(anotherMessage);

//Array Destructuring -> allows us to name array elements with variableNAmes instead of using the index number.
//Syntax -> let/const [variableName1, variableName2, variableName3] = arrayName;

const fullName = ["Juan", "Dela", "Cruz"];

//pre-Array Destructuring

console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);

//Hello Juan Dela Cruz!, It's nice to meet you.
console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you.`);

//Array Destructuring
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);

console.log(`Hello ${firstName} ${middleName} ${lastName}! It's nice to meet you.`);

//Object Destructuring

const person = {
    givenName: "Jane",
    maidenName: "Dela",
    surName: "Cruz"
}

//pre-Object Destructuring
console.log(person.givenName);
console.log(person.maidenName);
console.log(person.surName);

console.log(`Hello ${person.givenName} ${person.maidenName} ${person.surName}! It's good to see you again.`);

//Object Destructuring

const { givenName, maidenName, surName } = person;

/* console.log(givenName);
console.log(maidenName);
console.log(surName);

console.log(`Hello ${givenName} ${maidenName} ${surName}! It's nice to see you.`); */

function getFullName({ givenName, maidenName, surName }) {
    console.log(`Hello ${givenName} ${maidenName} ${surName}!`);
}

getFullName(person);

//Arrow Fucntion
//Syntax let/const variableName = () =>{ code block }
//Syntax let/const variableName = (parameter) =>{ code block }

/* function hello() {
    console.log("Hello World!");
} */


let hello = () => {
    console.log("Hello World!");
}

hello();

const students = ["John", "Jane", "Judy"];
/* 
students.forEach(function(student) {
    console.log(`${student} is a student.`);
}) */

students.forEach((student) => {
    console.log(`${student} is a student.`);
})

//Implicit Return Statement

/* const add = (x, y) => {
    return x + y;
} */

//Using implicit return
const add = (x, y) => x + y;


let total = add(1, 2);
console.log(total);

const numbers = [1, 2, 3, 4, 5];

let squareValues = numbers.map((numbers) => numbers ** 2);

console.log(squareValues);

//Default Funtion Arg Value

const greet = (name = "User") => `Good morning, ${name}`;
console.log(greet());
console.log(greet("John"));

//Class-Based Object Blueprints
/* Syntax -> 
class className{
    constructor(objectPropertyA, objectpropertyB){
        this.objectPropertyA = objectPropertyA;
        this.objectPropertyB = objectPropertyB;
    }
} */

class Car {
    constructor(brand, name, year) {
        this.brand = brand;
        this.name = name;
        this.year = year;

    }
}

const myCar = new Car();
console.log(myCar); //undefined no values

//Re-assign value of each property
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = "2021";

console.log(myCar); //with values here